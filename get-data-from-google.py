import os
import yaml
import sys

from google.oauth2 import service_account
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from googleapiclient.http import MediaIoBaseDownload



# If modifying these scopes, delete the file token.json.
SCOPES = [
    'https://www.googleapis.com/auth/spreadsheets.readonly',
    'https://www.googleapis.com/auth/drive.readonly',
    'https://www.googleapis.com/auth/drive.metadata.readonly',
]
SERVICE_ACCOUNT_FILE = 'google-api-service.json'

IMAGE_FOLDER_SLIDESHOW = '1ouqoxMkYYMnqx5ye_kC0TjbLZ7D63Z-y'
IMAGE_FOLDER_FOODTRUCK = '1vlx-qGM4EKkD8wgPhD77hPLYsG3idjPl'

# The ID and range of a sample spreadsheet.
SORTIMENT_SPREADSHEET_ID = '1KrEDT5MHAaHZRHM7fWR_ba4NBgB5_IDzY-D4NSzLBs0'
SORTIMENT_RANGE_NAME = 'Sortiment!A:T'
SORTIMENT_ALLERGENS_COLSTART = 7

TEXTS_SPREADSHEET_ID = '1KrEDT5MHAaHZRHM7fWR_ba4NBgB5_IDzY-D4NSzLBs0'
TEXTS_RANGE_NAME = 'Texty!A:C'

ROOT_DIR = "themes/google-drive-downloads"
DATA_DIR = "{}/data".format(ROOT_DIR)
ASSETS_DIR = "{}/assets".format(ROOT_DIR)
IMAGES_DIR = "{}/images".format(ASSETS_DIR)

ALLERGENS_YAML_FILE = "{}/allergens.yaml".format(DATA_DIR)
SORTIMENT_YAML_FILE = "{}/sortiment.yaml".format(DATA_DIR)
TEXTS_YAML_FILE = "{}/texts.yaml".format(DATA_DIR)

FOODTRUCK_DIR = "{}/foodtruck".format(IMAGES_DIR)
SLIDESHOW_DIR = "{}/slideshow".format(IMAGES_DIR)


credentials = service_account.Credentials.from_service_account_file(SERVICE_ACCOUNT_FILE, scopes=SCOPES)


def download_images(target_path, source_folder_id):
    print("Downloading images for {}".format(target_path))
    try:
        os.makedirs(target_path, exist_ok=True)
        drive_service = build('drive', 'v3', credentials=credentials)

        results = drive_service.files().list(q="'{}' in parents".format(source_folder_id), fields="files(id, name)").execute()
        image_files = results.get('files', [])

        for image_file in image_files:
            fileId = image_file.get('id')
            fileName = image_file.get('name')
            request = drive_service.files().get_media(fileId=fileId)
            target_file = open("{}/{}".format(target_path, fileName),'wb')
            downloader = MediaIoBaseDownload(target_file, request)
            print("Downloading {}".format(target_file.name))
            done = False
            while done is False:
                status, done = downloader.next_chunk()

    except HttpError as err:
        print(err)    

def download_sortiment():
    print("Downloading sortiment")
    try:
        service = build('sheets', 'v4', credentials=credentials)

        # Call the Sheets API
        sheet = service.spreadsheets()

        result = sheet.values().get(spreadsheetId=SORTIMENT_SPREADSHEET_ID,
                                    range=SORTIMENT_RANGE_NAME).execute()
        values = result.get('values', [])

        if not values:
            print('No data found.')
            return

        titles = values[0]
        allergens = []
        for algx, algname in enumerate(titles[SORTIMENT_ALLERGENS_COLSTART:]):
            allergen = {
                'code': "allergen-{}".format(algx),
                'name': algname
            }
            allergens.append(allergen)

        allergens_parent = { 'allergens': allergens }
        yaml.dump(allergens_parent, open(ALLERGENS_YAML_FILE, 'w'))          
        yaml.dump(allergens_parent, sys.stdout)

        categories = []
        for idx, row in enumerate(values[2:]):

            # skip empty rows
            if len(row) == 0 or len(row[0].strip()) == 0:
                continue

            is_category = row[3].lower().startswith('kat')
            is_off = row[3].lower().startswith('vyp')
            if is_category or is_off: # this is category
                section = row[0]
                name = row[1]
                menu_name = row[2]
                category = {
                    "section": section,
                    "name": name,
                    "menuName": menu_name,
                    "items": [],
                    "enabled": not is_off,
                    "showAllergens": True
                }
                if len(row) > 7:
                    if row[7] == 'X':
                        category['showAllergens'] = False

                categories.append(category)
                print(">> {} << ".format(category['name']))

            elif len(row[1]) > 0: # this is sortiment item
                print(", ".join(row))
                item = {
                    "number": row[0],
                    "name": row[1],
                    "description": row[2],
                    "price": row[3]
                }
                if len(row) > 4:
                    labels = []
                    if row[4] == 'TRUE':
                        labels.append("novinka")
                    if row[5] == 'TRUE':
                        labels.append("vegetarianska-pizza")    
                    if row[6] == 'TRUE':
                        labels.append("veganska-pizza")
                    if len(labels) > 0:
                        item['labels'] = labels                   

                if len(row) > 7:
                    allergens = []
                    for algx, algv in enumerate(row[SORTIMENT_ALLERGENS_COLSTART:]):
                        if algv == 'TRUE':
                            allergens.append("allergen-{}".format(algx))
                    if len(allergens) > 0:
                        item['allergens'] = allergens
                
                category['items'].append(item)

        categories_parent = { 'categories': categories }
        yaml.dump(categories_parent, open(SORTIMENT_YAML_FILE, 'w'))        
        yaml.dump(categories_parent)
 
    except HttpError as err:
        print(err)


def download_texts():
    print("Downloading texts")
    try:
        service = build('sheets', 'v4', credentials=credentials)

        # Call the Sheets API
        sheet = service.spreadsheets()
        result = sheet.values().get(spreadsheetId=TEXTS_SPREADSHEET_ID,
                                    range=TEXTS_RANGE_NAME).execute()
        values = result.get('values', [])

        texts = {}
        for row in values[1:]:
            code = row[0]
            textCell = row[2] if len(row) > 2 else '' 
            text = {
                "title": row[1],
                "text": textCell
            }
            texts[code] = text

        yaml.dump(texts, open(TEXTS_YAML_FILE, 'w'))        
        yaml.dump(texts, sys.stdout)
 
    except HttpError as err:
        print(err)



def main():
    """Shows basic usage of the Sheets API.
    Prints values from a sample spreadsheet.
    """
    os.makedirs(ROOT_DIR, exist_ok=True)
    os.makedirs(DATA_DIR, exist_ok=True)
    os.makedirs(ASSETS_DIR, exist_ok=True)
    os.makedirs(IMAGES_DIR, exist_ok=True)
    os.makedirs(FOODTRUCK_DIR, exist_ok=True)
    os.makedirs(SLIDESHOW_DIR, exist_ok=True)
    download_texts()
    download_sortiment()
    download_images(target_path=FOODTRUCK_DIR, source_folder_id= IMAGE_FOLDER_FOODTRUCK)
    download_images(target_path=SLIDESHOW_DIR, source_folder_id= IMAGE_FOLDER_SLIDESHOW)



if __name__ == '__main__':
    main()
